// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','ngStorage','ngCordova','ionic.ion.imageCacheFactory'])

.run(function($ionicPlatform,$rootScope,$state,$ionicHistory) {
	
	$rootScope.Host = "http://tapper.co.il/discreti/php/";
	$rootScope.LaravelHost = "http://tapper.co.il/discreti/discreti/public/";
	$rootScope.profilepicture = "";
	$rootScope.MatcheCounter = 0;
	$rootScope.matches = "";
	$rootScope.userid = "";
	//$rootScope.pushContent = "";
	$rootScope.currState = $state;
	
	$rootScope.FollowersCount = 0;
	$rootScope.FriendsCount = 0;
	$rootScope.BlockedCount = 0;


	$rootScope.$watch('currState.current.name', function(newValue, oldValue) {
      $rootScope.State = newValue;
    });  

	
	
  $ionicPlatform.ready(function() {
	  
	  


  
	  
  var notificationOpenedCallback = function(jsonData) {
	  
	  //alert (jsonData.additionalData.title);
	  //$rootScope.pushContent = jsonData.message;
	  
	  
	  //alert (555);
	 //alert (JSON.stringify(jsonData));
	  //alert (jsonData.additionalData.type);
	  
	  
	  $rootScope.$broadcast('newpushmsg',jsonData);
	  
	  
	  if (jsonData.additionalData.type == "newmessage")
	  {
		  $rootScope.$broadcast('newmessage',jsonData.additionalData);
		  $rootScope.$broadcast('refreshfriends',jsonData.additionalData);
		  
		  //$rootScope.$emit('refreshfriends');
		   
		  
		   if (!jsonData.isActive)
		   {
			   
			 $ionicHistory.nextViewOptions({
				disableBack: false,
				disableAnimate: true
			  });			   
			   $state.go('app.chat', { UserOne: jsonData.additionalData.userid, UserTwo: jsonData.additionalData.recipent });
			   
			   //window.location ="#/app/chat/"+jsonData.additionalData.user1+"/"+jsonData.additionalData.user2;
		   }
		   
		
		   
	  }

	  if (jsonData.additionalData.type == "newfriend")
	  {
		   $rootScope.$broadcast('newfriend',jsonData.additionalData);
		   if (!jsonData.isActive)
		   {
				 $ionicHistory.nextViewOptions({
					disableBack: false,
					disableAnimate: true
				  });
		  
			    $state.go('app.friends');
			   
			   //window.location = "#/app/friends";
		   }
	  }
	  
	  
	  if (jsonData.additionalData.type == "newfollower")
	  {
		   $rootScope.$broadcast('newfollower',jsonData.additionalData);
		   if (!jsonData.isActive)
		   {
				 $ionicHistory.nextViewOptions({
					disableBack: false,
					disableAnimate: true
				  });
		  
			    $state.go('app.followers');
			   
			   //window.location = "#/app/friends";
		   }
	  }
	  
	  
    console.log('didReceiveRemoteNotificationCallBack: ' + JSON.stringify(jsonData));
  };

  window.plugins.OneSignal.init("ab7a5e10-6075-48fc-a7b8-17b79fa7b7ab",
                                 {googleProjectNumber: "627358870772"},
                                 notificationOpenedCallback);

  window.plugins.OneSignal.getIds(function(ids) {
	
  $rootScope.pushId = ids.userId;	
  

	  
  
  //$scope.pushId = ids.userId;
	
	
   //alert (ids.userId)
   //alert (ids.pushToken);

				
  //console.log('getIds: ' + JSON.stringify(ids));
});

								 
  // Show an alert box if a notification comes in when the user is in your app.
  window.plugins.OneSignal.enableInAppAlertNotification(false);
  window.plugins.OneSignal.enableNotificationsWhenActive(false);
 
  
   


  
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {
	$ionicConfigProvider.backButton.previousTitleText(false).text('');
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl',
	//cache: false
  })


    .state('app.login', {
      url: '/login',
      views: {
        'menuContent': {
          templateUrl: 'templates/login.html',
          controller: 'LoginRegisterCtrl'
        }
      }
    })

    .state('app.settings', {
      url: '/settings',
      views: {
        'menuContent': {
          templateUrl: 'templates/settings.html',
          controller: 'SettingsCtrl',
		  cache: false,
        }
      }
    })


    .state('app.messages', {
      url: '/messages',
      views: {
        'menuContent': {
          templateUrl: 'templates/messages.html',
          controller: 'MessagesCtrl'
        }
      }
    })	
	
    .state('app.matches', {
      url: '/matches/:itemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/matches.html',
          controller: 'MatchesCtrl',
		  cache: false,
        }
      }
    })

    .state('app.chat', {
      url: '/chat/:UserOne/:UserTwo',
      views: {
        'menuContent': {
          templateUrl: 'templates/chat.html',
          controller: 'ChatCtr'
        }
      }
    })

	
	
    .state('app.followers', {
      url: '/followers',
      views: {
        'menuContent': {
          templateUrl: 'templates/followers.html',
          controller: 'FollowersCtrl'
        }
      }
    })


    .state('app.friends', {
      url: '/friends',
      views: {
        'menuContent': {
          templateUrl: 'templates/friends.html',
          controller: 'FriendsCtrl'
        }
      }
    })

    .state('app.blocked', {
      url: '/blocked',
      views: {
        'menuContent': {
          templateUrl: 'templates/blocked.html',
          controller: 'BlockedCtrl'
        }
      }
    })

    .state('app.dailer', {
      url: '/dailer',
      views: {
        'menuContent': {
          templateUrl: 'templates/dailer.html',
          controller: 'DialerCtrl'
        }
      }
    })
	
	
    .state('app.contact', {
      url: '/contact',
      views: {
        'menuContent': {
          templateUrl: 'templates/contact.html',
          controller: 'ContactCtrl'
        }
      }
    })	


    .state('app.payoptions', {
      url: '/payoptions',
      views: {
        'menuContent': {
          templateUrl: 'templates/payoptions.html',
          controller: 'PayOptionsCtrl'
        }
      }
    })	

	
;
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/login');
});
